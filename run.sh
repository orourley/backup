#!/usr/bin/env bash

# Get the remote and local
REMOTE_FOLDER=$1
LOCAL_FOLDER=$2

# Get the current script directory.
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Get the data as the folder name.
DATE=`date -I'date'`

# Run the backup.
$SCRIPT_DIR/../s3_sync/s3_sync.py backup $1/$DATE $2
