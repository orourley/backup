# Setup

Run these commands to install Python3 Pip3 & the boto3 module.

```
cd $HOME
# Install Pip for Python3
wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py

# Install the boto3 Python module.
python3 -m pip install boto3

# Get s3_sync.
git clone https://gitlab.com/orourley/s3_sync.git
cd s3_sync
```

Then copy the `key.txt` file from home computer to the `s3_sync/` folder.

# Running

It takes two args.

1. Remote folder in the form of `<S3 bucket>/<folder path without ending slash>`.
1. Local folder in the form of `<folder path without ending slash>`.

# Cron

To get it running regularly, add to cron. For example:

```
0 0 * * 0 $HOME/backup/run.sh orourley-backup/oak-server $HOME/oak-server/data
```
